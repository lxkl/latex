#!/usr/bin/env python3

import re

def pkg_(realname_, default_options_="", default_bool_="true", classes_blacklist_=[], additional_commands_=""):
	nickname_ = re.sub(r"[0-9-]", "", realname_.capitalize())
	bool_command_ = "\\lxklBasic" + nickname_
	options_command_ = "\\lxklBasic" + nickname_ + "Options"
	indent_ = ""
	print()
	if len(classes_blacklist_) > 0:
		print("\\makeatletter")
	for class_ in classes_blacklist_:
		print("%s\\@ifclassloaded{%s}{\\providecommand*%s{false}}{" % (indent_, class_, bool_command_))
		indent_ = indent_ + "  "
	print("%s\\providecommand*%s{%s}" % (indent_, bool_command_, default_bool_))
	for i_ in range(len(classes_blacklist_)):
		indent_ = indent_[2:]
		print("%s}" % (indent_))
	if len(classes_blacklist_) > 0:
		print("\\makeatother")
	print("\\providecommand*%s{%s}" % (options_command_, default_options_))
	print("\\str_if_eq:VnT %s {true}" % bool_command_)
	print("{")
	print("\\RequirePackage[%s]{%s}" % (options_command_, realname_))
	if len(additional_commands_) > 0:
		print(additional_commands_)
	print("}")

print("\\RequirePackage{expl3}")
print("\\ExplSyntaxOn")

## misc basics
pkg_("etoolbox")
pkg_("xifthen")
pkg_("substr")
pkg_("xstring")
pkg_("xparse")
pkg_("calc")
pkg_("microtype")
pkg_("babel", "russian,ngerman,english")
pkg_("xcolor", "svgnames,x11names,table", classes_blacklist_=["beamer", "dinbrief"])
pkg_("pagecolor", default_bool_="false")
pkg_("colortbl")
pkg_("mdframed")
pkg_("xspace")
pkg_("numprint", "autolanguage,np")
pkg_("siunitx")
pkg_("csquotes", "babel=true")
pkg_("blindtext")
pkg_("lipsum")
pkg_("setspace")
pkg_("relsize")
pkg_("moresize")
pkg_("changepage", "strict", classes_blacklist_=["memoir"])
pkg_("varwidth")
pkg_("enumitem", "ignoredisplayed")
pkg_("currfile")
pkg_("import")
pkg_("sectsty", default_bool_="false")
pkg_("tocloft", classes_blacklist_=["beamer"])
pkg_("footmisc", "bottom") # must be loaded early to ensure it is loaded before hyperref
pkg_("titlesec", "nobottomtitles*") # must be loaded early to ensure it is loaded before hyperref
pkg_("environ")
pkg_("suffix")
pkg_("printlen")
pkg_("needspace")
pkg_("chngcntr")

## symbols
pkg_("textcomp", "full")
pkg_("eurosym")
pkg_("ellipsis")
pkg_("bbding")
pkg_("wasysym", "nointegrals")
pkg_("gensymb", default_bool_="false")
pkg_("fontawesome")
pkg_("contour")

## changes
pkg_("fixme", "draft")
pkg_("changes", additional_commands_="\\normalem", default_bool_="false")
pkg_("datetime2")
pkg_("tagging")

## hyphenation
pkg_("hyphenat")
pkg_("extdash", "shortcuts")

## math
pkg_("amsmath")
pkg_("mathabx", "matha", default_bool_="false")
pkg_("amssymb")
pkg_("amsfonts")
pkg_("amstext")
pkg_("mathtools")
pkg_("nicefrac")
pkg_("units", default_bool_="false")
pkg_("dsfont")
pkg_("pifont")
pkg_("stmaryrd", default_bool_="false")
pkg_("aliascnt")
pkg_("amsthm")
pkg_("gauss")
pkg_("IEEEtrantools")
pkg_("spalign")
pkg_("blkarray")
pkg_("mathalfa", "bb=boondox", additional_commands_="\\DeclareMathAlphabet{\\boondoxmathbb}{U}{BOONDOX-ds}{m}{n}")

## floats
pkg_("algorithm2e", "tworuled,vlined,linesnumbered")
pkg_("listings")
pkg_("booktabs")
pkg_("longtable")
pkg_("tabu")
pkg_("tabularx")
pkg_("csvsimple")
pkg_("datatool")
pkg_("diagbox")
pkg_("multicol")
pkg_("multirow")
pkg_("placeins")
pkg_("floatrow", default_bool_="false")
pkg_("subfig", default_bool_="false")
pkg_("subcaption")
pkg_("adjustbox", "export")
pkg_("float")
pkg_("wrapfig")
pkg_("sidenotes", classes_blacklist_=["dinbrief", "memoir"])
pkg_("footnote", classes_blacklist_=["beamer"])
pkg_("cprotect")

pkg_("menukeys", additional_commands_="\\let\\mkdel\\del\\let\\del\\undefined") # must come after adjustbox

## referencing
pkg_("varioref")
pkg_("hyperref")
pkg_("bookmark")
pkg_("caption")
pkg_("acronym")
pkg_("url")
pkg_("biblatex", "style=numeric-comp, giveninits=true, maxnames=10")
pkg_("natbib", default_bool_="false")
pkg_("xr")

## graphics
pkg_("graphicx")
pkg_("grffile")
pkg_("eso-pic")
pkg_("pgfplots", additional_commands_="\\pgfplotsset{compat=1.18}")
pkg_("tikz", additional_commands_="""\\ExplSyntaxOff
  \\usetikzlibrary{positioning}
  \\usetikzlibrary{arrows}
  \\usetikzlibrary{shapes}
  \\usetikzlibrary{shadows}
  \\usetikzlibrary{decorations.text}
  \\usetikzlibrary{decorations.markings}
  \\usetikzlibrary{decorations.pathmorphing}
  \\usetikzlibrary{petri}
  \\usetikzlibrary{shapes.symbols}
  \\usetikzlibrary{shapes.arrows}
  \\usetikzlibrary{decorations}
  \\usetikzlibrary{decorations.pathreplacing}
  \\usetikzlibrary{decorations.shapes}
  \\usetikzlibrary{calc}
  \\usetikzlibrary{chains}
  \\usetikzlibrary{patterns}
  \\usetikzlibrary{matrix}
  \\usetikzlibrary{backgrounds}
  \\usetikzlibrary{mindmap}
  \\usetikzlibrary{topaths}
  \\usetikzlibrary{automata}
  \\usetikzlibrary{arrows.meta}
  \\ExplSyntaxOn""")
pkg_("tikzpagenodes")
pkg_("notespages")
pkg_("pdfpages")
pkg_("tcolorbox", "most")
pkg_("qrcode")

## page layout
pkg_("geometry")
pkg_("fancyhdr")
pkg_("lastpage")
## FIXME when compat problems with IEEEtrantools fixed, load cleveref by default
pkg_("cleveref", default_bool_="false") # looks like this must come after footmisc

print()
print("\ExplSyntaxOff")
