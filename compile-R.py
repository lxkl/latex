#!/usr/bin/env python3
import getopt, glob, joblib, math, multiprocessing, os, pathlib, re, subprocess, sys, time

color_msg_ = "\x1B[0;34m"
color_war_ = "\x1B[0;31m"
color_off_ = "\x1B[0m"

try:
	opts_, args_rest_ = getopt.getopt(sys.argv[1:], "fc:")
except getopt.GetoptError as err:
	print(str(err))
	print("usage: compile-R.py [-f] [-c interval]")
	sys.exit(2)
arg_force_rebuild_ = False
arg_continuous_interval_ = 0
for o, a in opts_:
	if o == "-f": arg_force_rebuild_ = True
	elif o == "-c": arg_continuous_interval_ = float(a)

def sedfile_(file_name_, re_1_, re_2_):
	tmp_file_name_ = file_name_ + ".{tmp}"
	with open(file_name_, "r") as file_:
		line_list_ = file_.readlines()
	with open(tmp_file_name_, "w") as file_:
		for line_ in line_list_:
			file_.write(re.sub(re_1_, re_2_, line_))
	os.rename(tmp_file_name_, file_name_)

def rmverdate_tex_(file_name_):
	sedfile_(file_name_, r"(% Created by tikzDevice) version .* on .*", r"\1")

def rmdate_pdf_(file_name_):
	tmp_file_name_ = file_name_ + ".{tmp}"
	subprocess.run(["pdftk", file_name_, "update_info", os.path.dirname(os.path.abspath(__file__)) + "/nodate.txt", "output", tmp_file_name_], check=True)
	os.rename(tmp_file_name_, file_name_)

def read_path_list_(file_path_):
	return [pathlib.Path(line_.rstrip("\n")) for line_ in open(str(file_path_)).readlines()]

def rebuild_if_needed_(compile_file_name_):
	compile_file_path_ = pathlib.Path(compile_file_name_)
	stdout_file_path_ = compile_file_path_.with_suffix(".stdout")
	compile_file_parent_path_ = compile_file_path_.parent
	output_file_name_list_ = [str(compile_file_parent_path_ / x_) for x_ in read_path_list_(compile_file_path_)] + [str(stdout_file_path_)]
	src_file_name_ = str(compile_file_path_.with_suffix(".R"))
	compile_gdeps_path_ = pathlib.Path("compile.gdeps")
	if os.path.isfile(str(compile_gdeps_path_)):
		dependency_file_name_list_ = read_path_list_(compile_gdeps_path_)
	else:
		dependency_file_name_list_ = []
	dependency_file_name_list_ += [src_file_name_, compile_file_name_]
	exists_rebuild_ = False
	if output_file_name_list_:
		do_rebuild_ = arg_force_rebuild_
		for output_file_name_ in output_file_name_list_:
			dirname_ = os.path.dirname(output_file_name_)
			if dirname_: os.makedirs(dirname_, exist_ok=True)
		if (not do_rebuild_):
			for output_file_name_ in output_file_name_list_:
				if (not os.path.isfile(output_file_name_)
					or any(os.path.getctime(output_file_name_) < os.path.getctime(dependency_file_name_)
						   for dependency_file_name_ in dependency_file_name_list_)):
					do_rebuild_ = True
					break
		if do_rebuild_:
			exists_rebuild_ = True
			rscript_env_ = os.environ.copy()
			rscript_env_["openin_any"] = "a"
			tmp_stdout_file_name_ = str(stdout_file_path_) + ".{tmp}"
			print(color_msg_ + "working: {}".format(src_file_name_) + color_off_)
			src_file_basename_ = os.path.basename(src_file_name_)
			start_time_ = time.time()
			with open(tmp_stdout_file_name_, "w") as stdout_file_:
				subprocess.run(["Rscript", src_file_basename_], check=True, env=rscript_env_, cwd=str(compile_file_parent_path_), stdout=stdout_file_)
			finish_time_ = time.time()
			compute_time_ = finish_time_-start_time_
			compute_time_mins_ = math.floor(compute_time_ / 60);
			compute_time_secs_ = round(compute_time_ - 60 * compute_time_mins_)
			os.rename(tmp_stdout_file_name_, str(stdout_file_path_))
			for output_file_name_ in output_file_name_list_:
				if os.path.isfile(output_file_name_):
					if output_file_name_.endswith(".tex"): rmverdate_tex_(output_file_name_)
					if output_file_name_.endswith(".pdf"): rmdate_pdf_(output_file_name_)
				else:
					print(color_war_ + "missing: {}: {}".format(src_file_name_, output_file_name_) + color_off_)
			print(color_msg_
				  + "   done: {} {:>4} sec ({:>3} min {:>2} sec)".format(src_file_name_, round(compute_time_), compute_time_mins_, compute_time_secs_)
				  + color_off_)
	return(exists_rebuild_)

def doit_():
	num_cores_ = multiprocessing.cpu_count()
	if not args_rest_:
		compile_list_ = glob.glob("**/*.compile", recursive=True)
	else:
		compile_list_ = args_rest_
	exists_rebuild_list_ = joblib.Parallel(n_jobs=num_cores_)(joblib.delayed(rebuild_if_needed_)(compile_file_name_)
								  for compile_file_name_ in compile_list_)
	if any(exists_rebuild_ for exists_rebuild_ in exists_rebuild_list_): print(color_msg_ + "rebuilds done" + color_off_)

if (arg_continuous_interval_ == 0): doit_()
elif (arg_continuous_interval_ > 0):
	while True:
		doit_()
		time.sleep(arg_continuous_interval_)
else: print("interval must be a non-negative number")
