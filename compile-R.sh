#!/bin/sh

## Just keep going, even in case of a crash! This functionality should
## be available as an option for compile-R.py, but I do not have time
## for that now.

compile_=`dirname $0`/compile-R.py
while true; do
    "${compile_?}" "$@"
done
